//==============================================================================================================================================
// Name        : genpass.cpp
// Author      : Slavick S (slavick.s.werewolf@gmail.com)
// Version     : 0.3
// Copyright   : BSD License 
//               Copyright (c) <2000>-<2024>, <Slavick S>
//               All rights reserved.
//               Поширення та використання у вихідній та бінарній формах, зі змінами або без, дозволяються за умови дотримання наступних умов:
//
//               1. Поширення вихідного коду повинно зберігати вищезазначене повідомлення про авторські права, 
//                  цей список умов та наступне застереження.
//
//               2. Поширення у бінарній формі повинно відтворювати вищезазначене повідомлення про авторські права, 
//                  цей список умов та наступне застереження у документації та/або інших матеріалах, 
//                  що надаються разом з розповсюдженням.
//
//               ЦЕ ПРОГРАМНЕ ЗАБЕЗПЕЧЕННЯ НАДАЄТЬСЯ АВТОРАМИ І ВЛАСНИКАМИ АВТОРСЬКИХ ПРАВ "ЯК Є" І БУДЬ-ЯКІ ВИСЛОВЛЕНІ АБО ПРИПУЩЕНІ ГАРАНТІЇ, 
//               ВКЛЮЧАЮЧИ, АЛЕ НЕ ОБМЕЖУЮЧИСЬ, ПРИПУЩЕНІ ГАРАНТІЇ ТОВАРНОЇ ПРИДАТНОСТІ ТА ПРИДАТНОСТІ ДЛЯ ПЕВНОЇ МЕТИ, ВІДХИЛЯЮТЬСЯ. 
//               НІ ЗА ЯКИХ ОБСТАВИН АВТОРИ АБО ВЛАСНИКИ АВТОРСЬКИХ ПРАВ НЕ НЕСУТЬ ВІДПОВІДАЛЬНОСТІ ЗА БУДЬ-ЯКІ ПРЯМІ, НЕПРЯМІ, ВИПАДКОВІ, 
//               СПЕЦІАЛЬНІ, ЗРАЗКОВІ АБО ПОБІЧНІ ЗБИТКИ (ВКЛЮЧАЮЧИ, АЛЕ НЕ ОБМЕЖУЮЧИСЬ, ПРИДБАННЯ ЗАМІННИХ ТОВАРІВ АБО ПОСЛУГ; 
//               ВТРАТУ ВИКОРИСТАННЯ, ДАНИХ АБО ПРИБУТКІВ; АБО ПЕРЕРИВАННЯ ДІЯЛЬНОСТІ), НЕЗАЛЕЖНО ВІД ПРИЧИНИ І ТЕОРІЇ ВІДПОВІДАЛЬНОСТІ, 
//               ЧИ БУЛИ ВОНИ УГОДОЮ, СУВОРОЮ ВІДПОВІДАЛЬНІСТЮ АБО ДЕЛІКТОМ (ВКЛЮЧАЮЧИ НЕДБАЛІСТЬ АБО ІНШІ ДІЇ), 
//               ЩО ВИНИКАЮТЬ У БУДЬ-ЯКИЙ СПОСІБ ІЗ ВИКОРИСТАННЯМ ЦЬОГО ПРОГРАМНОГО ЗАБЕЗПЕЧЕННЯ, НАВІТЬ ЯКЩО ПОПЕРЕДЖЕНІ ПРО МОЖЛИВІСТЬ ТАКИХ ЗБИТКІВ.
//
// Description : Програма генерує паролі довільної довжини, але не менше 10 символів, та зберігає їх у файлі.
//               Скомпільовано під Debian GNU/Linux 12 (bookworm) : clang++ genpass.cpp -m64 -O3 -Wall -Werror -pedantic -std=c++20 -o genpass
//               clang++ --version : 14.0.6
//               Скомпільовано під macOS 14.4 Sonoma : g++ genpass.cpp -m64 -O3 -Wall -Werror -pedantic -std=c++20 -o genpass
//               g++ --version : Apple clang version 15.0.0 (clang-1500.3.9.4) 
//
//
//==============================================================================================================================================
//

#include <iostream>
#include <fstream>
#include <random>
#include <string>
#include <vector>
#include <limits>

// Функція для генерації випадкового пароля
std::string generate_password(int length, std::mt19937& gen) {
    const std::string characters = 
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "!@#$%^&*()-_+=<>?";

    std::uniform_int_distribution<> dis(0, characters.size() - 1);

    std::string password;
    for (int i = 0; i < length; ++i) {
        password += characters[dis(gen)];
    }

    return password;
}

// Функція для збереження паролів у файл
void save_passwords_to_file(const std::vector<std::string>& passwords, const std::string& filename) {
    std::ofstream file(filename);
    if (file.is_open()) {
        for (const auto& password : passwords) {
            file << password << "\n";
        }
        file.close();
    } else {
        std::cerr << "Не вдалося відкрити файл!!! Можливо диск заповнено, захищено від запису або у Вас немає дозволу на створення або зміну файлів:" << filename << std::endl;
    }
}

int main() {
    int password_length, number_of_passwords;
    std::string filename;
    
    std::cout << "**********************************************************" << std::endl;
    std::cout << "*                                                        *" << std::endl;
    std::cout << "*                    Слава Україні!!!                    *" << std::endl;
    std::cout << "*                                                        *" << std::endl;
    std::cout << "*         Вітаємо у програмі генерації паролей!          *" << std::endl;
    std::cout << "*                                                        *" << std::endl;
    std::cout << "* Password generator by Slavick S aka MaD-DoG2000 v 0.3  *" << std::endl;
    std::cout << "*                                                        *" << std::endl;
    std::cout << "**********************************************************" << std::endl;
    std::cout << "" << std::endl;
    
    // Перевірка наявності апаратного генератора випадкових чисел
    std::random_device rd;
    std::mt19937 gen;

    // Якщо апаратний генератор не доступний, використовуємо програмний
    if (rd.entropy() > 0) {
        std::cout << "Використовується апаратний генератор випадкових чисел!"  << std::endl;
        std::cout << "" << std::endl;
        gen.seed(rd()); // Використовуємо випадкове число як початкове для апаратного генератора
    } else {
        std::cout << "Апаратний генератор випадкових чисел відсутній! Використовується програмний генератор."  << std::endl;
        std::cout << "" << std::endl;
        std::random_device fallback;
        gen.seed(fallback());  // Використовуємо випадкове число як початкове для програмного генератора
    }

    // Меню для вводу параметрів
    std::cout << "Введіть бажану довжину пароля: ";
    std::cin >> password_length;
    while (std::cin.fail() || password_length <= 9) { 
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Довжиу пароля введено некоректно. Будь ласка введіть бажану довжину пароля знову: ";
        std::cin >> password_length;
                                                    }
    
    std::cout << "Введіть кількість паролів для генерації: ";
    std::cin >> number_of_passwords;
    while (std::cin.fail() || number_of_passwords <= 0) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Будь ласка, введіть коректне значення для кількості паролів: ";
        std::cin >> number_of_passwords;
                                                        }

    std::cout << "Введіть ім'я файлу для збереження паролів: ";
    std::cin >> filename;

    std::vector<std::string> passwords;
    for (int i = 0; i < number_of_passwords; ++i) {
        passwords.push_back(generate_password(password_length, gen));
                                                  }

    save_passwords_to_file(passwords, filename);

    std::cout << "Паролі згенеровано та збережено у файл " << filename << std::endl;

    return 0;
}

